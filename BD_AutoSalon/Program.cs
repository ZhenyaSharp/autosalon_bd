﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD_AutoSalon
{
    class Program
    {
        static int CURRENT_ID = 0;

        struct Car
        {
            public int Id;
            public string CarModel;
            public string CarBody;
            public int YearCarManufacture;
            public double EngineCapacity;
            public string Transmission;
            public int Balance;
            public int Price;
        }
        #region System Methonds

        static void ResizeArray(ref Car[] cars, int newLength)
        {
            int minLength = newLength > cars.Length ? cars.Length : newLength;
            Car[] newArray = new Car[newLength];

            for (int i = 0; i < minLength; i++)
            {
                newArray[i] = cars[i];
            }
            cars = newArray;
        }

        static int InputInt(string message)
        {
            bool inputResult;
            int numInt;
            do
            {
                Console.Write(message);
                inputResult = int.TryParse(Console.ReadLine(), out numInt);
            } while (!inputResult);

            return numInt;
        }

        static double InputDouble(string message)
        {
            bool inputResult;
            double numDouble;
            do
            {
                Console.Write(message);
                inputResult = double.TryParse(Console.ReadLine(), out numDouble);
            } while (!inputResult);

            return numDouble;
        }

        static bool InputBool(string message)
        {
            bool inputResult;
            bool b;

            do
            {
                Console.Write(message);
                inputResult = bool.TryParse(Console.ReadLine(), out b);
            } while (!inputResult);

            return b;
        }

        static string InputString(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }
        #endregion

        #region CRUDS Methonds

        static void AddNewCars(ref Car[] cars, Car car)
        {

            if (cars == null)
            {
                cars = new Car[1];
            }
            else
            {
                ResizeArray(ref cars, cars.Length + 1);
            }
            cars[cars.Length - 1] = car;
        }

        static void DeleteCarByID(ref Car[] cars, int id)
        {
            int indexDelete = GetIndexByID(cars, id);
            if (indexDelete == -1)
            {
                Console.WriteLine("Удаление невозможно.Элемент не найден");
                return;
            }
            Car[] newCars = new Car[cars.Length - 1];
            int newI = 0;

            for (int i = 0; i < cars.Length; i++)
            {
                if (i != indexDelete)
                {
                    newCars[newI] = cars[i];
                    newI++;
                }
            }

            cars = newCars;
        }

        static void ClearAll(ref Car[] cars)
        {
            cars = null;
        }

        static void UpdateCarByID(Car[] cars, int id, Car car)
        {
            int indexUpdate = GetIndexByID(cars, id);

            if (indexUpdate == -1)
            {
                Console.WriteLine("Обновление невозможно.Элемент не найден");
                return;
            }

            car.Id = cars[indexUpdate].Id;
            cars[indexUpdate] = car;
        }

        static void InsertCarIntoPosition(ref Car[] cars, int position, Car car)
        {
            if (cars == null)
            {
                Console.WriteLine("Ошибка.Массив пуст");
                return;
            }

            if (position < 1 || position > cars.Length)
            {
                Console.WriteLine("Ошибка. Позиция не найдена");
                return;
            }

            int indexInsert = position - 1;

            Car[] newCars = new Car[cars.Length + 1];
            int oldI = 0;

            for (int i = 0; i < newCars.Length; i++)
            {
                if (i != indexInsert)
                {
                    newCars[i] = cars[oldI];
                    oldI++;
                }
                else
                {
                    newCars[i] = car;
                }
            }

            cars = newCars;
        }

        #endregion

        #region Tools Methods

        static int GetMinPrice(Car[] cars)
        {
            int minPrice = cars[0].Price;

            for (int i = 0; i < cars.Length; i++)
            {
                if (cars[i].Price < minPrice)
                {
                    minPrice = cars[i].Price;
                }
            }

            return minPrice;
        }

        static int GetMaxPrice(Car[] cars)
        {
            int maxPrice = cars[0].Price;

            for (int i = 0; i < cars.Length; i++)
            {
                if (cars[i].Price > maxPrice)
                {
                    maxPrice = cars[i].Price;
                }
            }

            return maxPrice;
        }

        static int GetMinYearManufacture(Car[] cars)
        {
            int minYear = cars[0].YearCarManufacture;
            for (int i = 0; i < cars.Length; i++)
            {
                if (cars[i].YearCarManufacture < minYear)
                {
                    minYear = cars[i].YearCarManufacture;
                }
            }

            return minYear;
        }

        static int GetMaxYearManufacture(Car[] cars)
        {
            int maxYear = cars[0].YearCarManufacture;
            for (int i = 0; i < cars.Length; i++)
            {
                if (cars[i].YearCarManufacture > maxYear)
                {
                    maxYear = cars[i].YearCarManufacture;
                }
            }

            return maxYear;
        }

        static int GetIndexByID(Car[] cars, int id)
        {
            if (cars == null)
            {
                return -1;
            }
            for (int i = 0; i < cars.Length; i++)
            {
                if (cars[i].Id == id)
                {
                    return i;
                }
            }

            return -1;
        }

        static Car CreateCar(bool isNewId)
        {
            Car car;
            if (isNewId)
            {
                CURRENT_ID++;
                car.Id = CURRENT_ID;
            }
            else
            {
                car.Id = 0;
            }

            car.CarModel = InputString("Введите марку автомобиля: ");

            car.CarBody = InputString("Введите модель (тип кузова) автомобиля: ");

            car.YearCarManufacture = InputInt("Введите год производства: ");

            car.EngineCapacity = InputDouble("Введите объем двигателя: ");

            car.Transmission = InputString("Введите тип коробки передач: ");

            car.Balance = InputInt("Введите кол-во автомобилей:  ");

            car.Price = InputInt("Введите стоимость автомобиля: ");

            return car;
        }

        static Car CreateEmptyCar()
        {
            Car car;
            car.Id = 0;
            car.CarModel = "";
            car.CarBody = "";
            car.YearCarManufacture = 0;
            car.EngineCapacity = 0;
            car.Transmission = "";
            car.Balance = 0;
            car.Price = 0;
            return car;
        }

        static void PrintCar(Car car)
        {
            Console.WriteLine("{0,-4}{1,-15}{2,-15}{3,-8}{4,-12}{5,-10}{6,-4}{7,-10}", car.Id, car.CarModel, car.CarBody,
                car.YearCarManufacture, car.EngineCapacity, car.Transmission, car.Balance, car.Price);
        }

        static void PrintManyCars(Car[] cars)
        {
            Console.WriteLine("{0,-4}{1,-15}{2,-15}{3,-8}{4,-12}{5,-10}{6,-4}{7,-10}", "ИД", "Марка Авто",
                "Модель Авто", "Год", "Об.Двиг", "КПП", "Ост", "Цена");

            if (cars == null)
            {
                Console.WriteLine(("Массив пуст"));
            }
            else if (cars.Length == 0)
            {
                Console.WriteLine(("Массив пуст"));
            }
            else
            {
                for (int i = 0; i < cars.Length; i++)
                {
                    PrintCar(cars[i]);
                }
            }

            Console.WriteLine("********************************");
        }


        #endregion

        #region RetriveMethods

        static bool FindCarByID(Car[] cars, int id, out Car car)
        {
            int indexPrint = GetIndexByID(cars, id);

            if (indexPrint == -1)
            {
                car = CreateEmptyCar();
                return false;
            }
            else
            {
                car = cars[indexPrint];
                return true;
            }
        }

        static Car[] FindCarsFromMinToMaxPrice(Car[] cars, int minPrice, int maxPrice)
        {
            Car[] findedCars = null;

            for (int i = 0; i < cars.Length; i++)
            {
                if (cars[i].Price >= minPrice && cars[i].Price <= maxPrice)
                {
                    AddNewCars(ref findedCars, cars[i]);
                }
            }

            return findedCars;
        }

        static Car[] FindCarsFromMinToMaxYearManufacture(Car[] cars, int minYear, int maxYear)
        {
            Car[] findedCars = null;

            for (int i = 0; i < cars.Length; i++)
            {
                if (cars[i].YearCarManufacture >= minYear && cars[i].YearCarManufacture <= maxYear)
                {
                    AddNewCars(ref findedCars, cars[i]);
                }
            }

            return findedCars;
        }

        #endregion

        #region SortMethods

        static void SortCarByBalance(Car[] cars, bool asc)
        {
            Car temp;
            bool sort = true;
            int offset = 0;

            do
            {
                sort = true;

                for (int i = 0; i < cars.Length - 1 - offset; i++)
                {
                    bool compareResult;

                    if (asc)
                    {
                        compareResult = cars[i + 1].Balance < cars[i].Balance;
                    }

                    else
                    {
                        compareResult = cars[i + 1].Balance > cars[i].Balance;
                    }

                    if (compareResult)
                    {
                        temp = cars[i];
                        cars[i] = cars[i + 1];
                        cars[i + 1] = temp;
                        sort = false;
                    }

                }
                offset++;
            } while (!sort);
        }

        static void SortCarByID(Car[] cars, bool asc)
        {
            Car temp;
            bool sort = true;
            int offset = 0;

            do
            {
                sort = true;

                for (int i = 0; i < cars.Length - 1 - offset; i++)
                {
                    bool compareResult;

                    if (asc)
                    {
                        compareResult = cars[i + 1].Id < cars[i].Id;
                    }

                    else
                    {
                        compareResult = cars[i + 1].Id > cars[i].Id;
                    }

                    if (compareResult)
                    {
                        temp = cars[i];
                        cars[i] = cars[i + 1];
                        cars[i + 1] = temp;
                        sort = false;
                    }

                }
                offset++;
            } while (!sort);
        }

        #endregion

        #region  Interface Methods

        static void PrintMenu()
        {
            Console.WriteLine("1.  Добавить новый автомобиль");
            Console.WriteLine("2.  Удалить автомобиль по айди");
            Console.WriteLine("3.  Удаление всех автомобилей");
            Console.WriteLine("4.  Обновление автомобиля по айди");
            Console.WriteLine("5.  Вставка автомобиля на любую позицию");
            Console.WriteLine("6.  Поиск автомобиля по айди");
            Console.WriteLine("7.  Поиск автомобилей по годам производства");
            Console.WriteLine("8.  Поиск автомобилей по стоимости");
            Console.WriteLine("9.  Сортировка автомобилей по количеству");
            Console.WriteLine("10. Сортировка автомобилей по айди");
            Console.WriteLine("0.  Выход");
        }

        #endregion

        static void Main(string[] args)
        {
            Car[] cars = null;
            bool runProgram = true;

            while (runProgram)
            {
                Console.Clear();
                PrintManyCars(cars);

                PrintMenu();
                int menuPoint = InputInt("Введите пункт меню: ");

                switch (menuPoint)
                {
                    case 1:
                        {
                            Car addedCar = CreateCar(true);
                            AddNewCars(ref cars, addedCar);
                        }
                        break;

                    case 2:
                        {
                            int id = InputInt("Введите айди удаляемого автомобиля: ");
                            DeleteCarByID(ref cars, id);
                        }
                        break;

                    case 3:
                        {
                            ClearAll(ref cars);
                        }
                        break;

                    case 4:
                        {
                            int id = InputInt("Введите айди обновляемого автомобиля: ");
                            Car car = CreateCar(false);
                            UpdateCarByID(cars, id, car);
                        }
                        break;

                    case 5:
                        {
                            int position = InputInt("Введите позицию для вставки: ");
                            Car car = CreateCar(true);
                            InsertCarIntoPosition(ref cars, position, car);
                        }
                        break;

                    case 6:
                        {
                            int id = InputInt("Введите айди нужного автомобиля");
                            Car car;
                            bool isFinded = FindCarByID(cars, id, out car);

                            if (isFinded)
                            {
                                PrintCar(car);
                            }
                            else
                            {
                                Console.WriteLine("Ошибка. Автомобиль не найден");
                            }
                        }
                        break;


                    case 7:
                        {
                            Console.WriteLine(
                                $"Введите диапазон поиска от {GetMinYearManufacture(cars)} до {GetMaxYearManufacture(cars)}");

                            int minYear = InputInt("минимальный год производства: ");

                            int maxYear = InputInt("максимальный год производства: ");

                            Car[] findedCars = FindCarsFromMinToMaxYearManufacture(cars, minYear, maxYear);
                            PrintManyCars(findedCars);
                        }
                        break;

                    case 8:
                        {
                            Console.WriteLine(
                                $"Введите диапазон поиска от {GetMinPrice(cars)} до {GetMaxPrice(cars)}");

                            int minPrice = InputInt("минимальная стоимость: ");

                            int maxPrice = InputInt("максимальная стоимость: ");

                            Car[] findedCars = FindCarsFromMinToMaxPrice(cars, minPrice, maxPrice);
                            PrintManyCars(findedCars);
                        }
                        break;

                    case 9:
                        {
                            bool asc = InputBool("Сортировка по возрастанию?(true/false)");
                            SortCarByBalance(cars, asc);
                        }
                        break;

                    case 10:
                    {
                        bool asc = InputBool("Сортировка по возрастанию?(true/false)");
                        SortCarByID(cars, asc);
                    }
                        break;


                    case 0:
                        {
                            Console.WriteLine("Программа будет закрыта");
                            runProgram = false;
                        }
                        break;

                    default:
                        {
                            Console.WriteLine("Неверный пункт меню");
                        }
                        break;
                }

                Console.ReadKey();
            }

        }
    }
}
